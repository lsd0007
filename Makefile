all: txt html

html:
	xml2rfc --html --css style.css draft-gnunet-communicators.xml

txt:
	xml2rfc draft-gnunet-communicators.xml

